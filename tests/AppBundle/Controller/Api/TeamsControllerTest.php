<?php

namespace Tests\AppBundle\Controller\Api;

use Tests\AppBundle\Test\ApiTestCase;
use AppBundle\Controller\TeamsController;

class TeamsControllerTest extends ApiTestCase
{
    public function testList()
    {
        $token = $this->getService('lexik_jwt_authentication.encoder')
            ->encode(['username' => 'mehersushil@gmail.com']);

        $response = $this->client->get($this->client->getConfig('base_url') . "/api/teams/1", [
            'headers' => [
                'Authorization' => 'Bearer'.$token
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $finishData = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('league', $finishData);
    }
}
