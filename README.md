football
========

1. Setup database credentials in parameters.yml
2. Go to project folder and run
``composer insall``
3. Run ``./bin/console doctrine:database:create``
4. Run ``./bin/console doctrine:migrations:migrate``
5. Run ``./bin/console doctrine:fixtures:load``

EndPoints:
1. /api/teams/{leagueId} Method: GET
2. /api/leagues/{leagueId} Method: DELETE

Username: mehersushil@gmail.com
Password: 12345