<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\League;
use AppBundle\Entity\Team;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TeamsController
 * @package AppBundle\Controller\Api
 * @Security("is_granted('ROLE_USER')")
 */
class TeamsController extends Controller
{
    /**
     * @Route("/api/teams/{league}", methods={"GET"})
     * @param League $league
     * @return JsonResponse
     */
    public function listAction(League $league)
    {
        $teams = $league->getTeams();
        $data = [
            'league' => [
                'name' => $league->getName(),
                'teams' => [],
            ]
        ];
        foreach ($teams as $team) {
            $data['league']['teams'][] = $this->serializeTeams($team);
        }

        return new JsonResponse($data);
    }

    private function serializeTeams(Team $team) {
        return [
            'name' => $team->getName(),
            'strip' => $team->getStrip()
        ];
    }
}