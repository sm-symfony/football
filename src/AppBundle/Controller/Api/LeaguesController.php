<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\League;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LeaguesController
 * @package AppBundle\Controller\Api
 * @Security("is_granted('ROLE_USER')")
 */
class LeaguesController extends Controller
{
    /**
     * @Route("/api/leagues/{league}", methods={"DELETE"})
     * @param League $league
     * @return JsonResponse
     */
    public function deleteAction(League $league)
    {
        if (!$league) {
            throw $this->createNotFoundException("No league found for name " . $league);
        }

        if ($league) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($league);
            $em->flush();
        }

        return new JsonResponse(null, 204);
    }
}