<?php


namespace AppBundle\Datafixtures\ORM;


use AppBundle\Entity\League;
use AppBundle\Entity\Team;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $leaguesAndTeams = [
            'FA Premier League' => ['Manchester United', 'Arsenal', 'Chelsea', 'Liverpool'],
            'League 1' => ['Olympique Lyonnais', 'Olympique de Marseille', 'AS Monaco', 'Paris Saint-Germain'],
            'Bundesliga' => ['FC Bayern München', 'Borussia Dortmund', 'Bayer Leverkusen', 'Schalke 04', 'SV Werder Bremen'],
            'Serie A' => ['Juventus', 'AC Milan', 'Inter Milan'],
            'La Liga' => ['Real Madrid', 'FC Barcelona']
        ];

        // create 5 leagues!
        foreach ($leaguesAndTeams as $leagueName => $teamName) {
            $league = new League();
            $league->setName($leagueName);
            $manager->persist($league);
        }
        $manager->flush();

        $leagues = $manager->getRepository('AppBundle:League')
            ->findAll();

        foreach ($leagues as $league) {
            $teams = $leaguesAndTeams[$league->getName()];
            foreach ($teams as $teamName) {
                $team = new Team();
                $team->setName($teamName);
                $team->setStrip($faker->word);
                $team->setLeague($league);
                $manager->persist($team);
            }
        }
        $manager->flush();

        $user = new User();
        $user->setEmail('mehersushil@gmail.com');
        $user->setPlainPassword('12345');
        $user->setRoles(['ROLE_USER']);
        $manager->persist($user);
        $manager->flush();
    }
}